# Projet_CICD

# Table des matières
1. [Doc de présentation](#Doc-de-présentation)
2. [Ressources](#Ressources)
2. [Doc d'installation](#Doc-d'installation)
3. [Choses à terminer](#Ce-que-je-n'ai-pas-fini)

## 1. Doc de présentation
La solution présente plusieurs éléments tous hébergés sur le cloud azure :
- Un Gitlab afin de stocker le code des applications
- Une base de données PostgreSQL pour les données relatives aux applications
- Un container registry afin de stocker les images docker des applications
- Un cluster Kubernetes afin de faire tourner les runners Gitlab ainsi que les applications conteneurisées.

On peut présenter l'architecture finale avec le schéma suivant :
```mermaid
graph TD
    PC -->|Push Code| Gitlab
    Pipeline --> |Update Registry| R
    Pipeline --> |Run pipeline| Runner
    C --> App(App release)
    Gitlab -->  Pipeline
    R(Container Registry Azure) --> |update app| App
    C{Cluster Kubernetes} --- Runner(Runners GitLab)
    App --- Database((Database))
```
## 2. Ressources
Dépôt de  [l'API](https://gitlab.com/Enigmind/api-projet).

Dépôt du front (même si pas très utile celui là) : [doc officielle](https://gitlab.com/Enigmind/tp1-front).
## 3. Doc d'installation

### *Partie applicative :*
Commençons par détailler l'application. Elle se compose d'une API rédigée en python ainsi que d'une vue en React.js qui accède à l'API. L'API communique avec la base de donnée via l'ORM de Django (framework Python).
#### __API :__
L'API est simple car ça ne représentait pas le coeur du pojet en lui même. J'ai simplement codé le squelette ainsi que l'accès à la base de données précédemment créée sur Azure. (Je me permets de laisser le mot de passe en clair car les resources seront détruites avant d'avoir commit)
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'ils_db',
        'USER': 'postgres@ilsdatabase',
        'PASSWORD': 'QYz!JQ8cnVTg@87',
        'HOST': 'ilsdatabase.postgres.database.azure.com',
        'PORT': 5432,
    }
}
```
Le Dockerfile suivant permet de conteneriser l'API :
```Dockerfile
# pull official base image
FROM python:3.8.3-alpine

# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install psycopg2 dependencies
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r requirements.txt

# copy project
COPY . .
```
Suivi du docker-compose pour l'éxécuter :
```yml
version: '3.7'

services:
  api:
    image: ils_api
    build:
      context: .
      dockerfile: Dockerfile
    command: python manage.py runserver 0.0.0.0:8000
    volumes:
      - ./projet_TDD/:/usr/src/projet_TDD/
    ports:
      - 8000:8000
    container_name: ils_api
```
(Ce docker-compose reste pour le dev actuellement. Je suis en train d'en faire un pour la prod avec un reverse proxy au lieu d'utiliser le serveur de dev de Django)

#### Pipeline :
La pipeline est lancée grâce au fichier .gitlab-ci.yml suivant :
```yml
image: "python:3.7"

# install dependences
before_script:
  - pip install -r requirements.txt

stages:
  - Lint
  - Test
  - Deploy

# test code linting
pylint:
  stage: Lint
  script:
  - pylint -d C,E1101,W0703,R APILS/tests.py APILS/models.py APILS/views.py projet_TDD/urls.py projet_TDD/serializers.py projet_TDD/settings.py

  # run unit tests
test_api:
  stage: Test
  script:
  - python manage.py test

production:
  stage: Deploy
  script:
    - echo "Deploy to production"
  environment:
    name: production
    url: https://myaksclust-prodthales-584502-89d1cb17.hcp.westeurope.azmk8s.io:443
  only:
    - master
```

On a donc 3 stages dont 2 se lancent peu importe la branche et une (deploy) qui se lance uniquement sur la branche master (pour le déploiement en production donc).
Le job production ne fonctionne pas encore et je n'ai pas eu le temps de le terminer. Il parvient à se connecter au cluster kube mais il faut encore arriver à le faire build puis push l'image docker sur le registry.

#### __Frontend :__
Je n'ai pas eu le temps de faire un front de qualité vu que là aussi c'était pas le coeur du projet donc j'ai juste utilisé un projet React vide pour simuler une app web.
Voici quand même le Dockerfile :
```Dockerfile
# build environment
FROM node:13.12.0-alpine as build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
COPY package-lock.json ./
RUN npm ci --silent
RUN npm install react-scripts@3.4.1 -g --silent
COPY . ./
RUN npm run build

# production environment
FROM nginx:stable-alpine
COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

```

Et le docker-compose :
```yml
version: '3.7'

services:

  sample-prod:
    container_name: front-prod
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - '1337:80'

```
#### Pipeline :
Je n'ai pas encore créé de pipeline pour le front vu qu'il est "vide". Mais je le ferai lors de la mise en place de ce projet dans mon environnement professionnel.



### *Partie Infra :*
L'infrastructure se compose donc d'une base de données, d'un GitLab, d'un container registry ainsi que d'un cluster kubernetes.
Tout est hébergé sur le cloud azure.

#### **Container registry**
Le container registry est un registry azure (ACR). Il a été créé suivant la [doc officielle](https://docs.microsoft.com/fr-fr/azure/aks/tutorial-kubernetes-prepare-acr). Il stocke pour le moment les dernières versions du front et de l'API comme on le voir sur l'output de la commande suivante :
![alt text](img/images_docker.png "imgs")

#### **Kubernetes**
Le cluster a été créé suivant la [doc officielle](https://docs.microsoft.com/fr-fr/azure/aks/tutorial-kubernetes-deploy-cluster). Il a été connecté au registry via la commande suivante :
```bash
az aks update -n myAKSCluster -g Prod_Thales --attach-acr RegistryThales
```

#### **GitLab**
Le GitLab a été créé via l'image fournie par Azure cf screen suivant :
![alt text](img/git.png "git")
Ce fut une colossale erreur de ma part car ces images propriétaires sont verouillées jusqu'à la moelle et toucher à la conf interne est vraiment un pain dans le *** comme disent nos amis amerloques. Bref, le jour où je refais ce projet pour une vraie prod, je prendrai soin de monter un gitlab moi même via une image ubuntu/debian/centOS vierge.

**Connexion au cluster Kube** :

La connexion se fait via un formulaire en GUI. auquel il faut soumettre toutes sortes d'informations comme le token et le certificat du cluster que l'on souhaite connecter. Une fois chose faite, on peut installer toutes sortes d'application depuis l'interface GitLab qui tourneront dans des pods du cluster.

![alt text](img/pods_kube.png "pods")

On voit sur cette image que tournent dans des pods les applications ingress, prometheus et gitlab runner dont on va parler tout de suite.
On peut aussi voir depuis gitlab les metrics du cluster grâce à prometheus installé dessus :

![alt text](img/kube_health.PNG "health")

**GitLab Runners** :

Une fois l'application runners installée sur le cluster, nos runners peuvent désormais tourner dessus plutôt que sur Gitlab !

![alt text](img/runner.PNG "runner")

Grâce aux pipelines rédigées dans le `.gitlab-ci.yml`, nos jobs s'éxécutent bien sur le cluster :

![alt text](img/runner_in_kube.PNG "pipe")

## 4. Ce que je n'ai pas fini

Pour l'instant, la pipeline ne construit pas l'image docker et ne l'ajoute pas au registry. Une fois que cela sera fait, il faudra faire en sorte que dès que l'image dans le registry a une nouvelle version, le conteneur se met à jour avec celle-ci.

